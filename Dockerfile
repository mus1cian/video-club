FROM node
LABEL Valeria Nevarez
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
