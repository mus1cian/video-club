const express = require('express');


function list(req, res, next) {
    res.send('respond with list');
}

function index(req, res, next) {
    const id = req.params.id;
    res.send(`respond with index >>${id}`);
}


function create(req, res, next) {
    res.send('respond with create');
}

function replace(req, res, next) {
    res.send('respond with replace');
}

function update(req, res, next) {
    res.send('respond with update');
}

function destroy(req, res, next) {
    res.send('respond with destroy');
}


module.exports = {
    list,
    index,
    create,
    replace,
    update,
    destroy
};
